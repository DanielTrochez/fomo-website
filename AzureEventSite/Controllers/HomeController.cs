﻿using AzureEventSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Twilio;
using System.Configuration;
using System.Net;
using System.Net.Mail;

namespace AzureEventSite.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
               
                List<EventDisplayModel> events = new List<EventDisplayModel>();
                using (var context = new AzureTentanEntities())
                {

                    var allEvents = context.Events.ToList().OrderByDescending(q => q.ID);

                    foreach (var e in allEvents)
                    {
                        var eventToAdd = new EventDisplayModel
                        {
                            Id = e.ID,
                            Name = e.Name,
                            Date = e.Date,
                            Description = e.Description,
                            Pic = e.Picture
                        };
                        events.Add(eventToAdd);
                    }
                }

                return View(events);
            }
            else
            {
                return RedirectToAction("LogIn", "User");
            }
        }


        public ActionResult CreateEvent()
        {
            if (Request.IsAuthenticated)
            {
                return PartialView("_CreateEvent");
            }
            else
            {
                return RedirectToAction("LogIn", "User");
            }
          
        }

        [HttpPost]
        public ActionResult CreateEvent(EventPropHolder propholder, string date, HttpPostedFileBase picture)
        {

            EventController eventController = new EventController();
            propholder.Picture = picture;
            propholder.Date = date;

            eventController.Post(propholder);

            return RedirectToAction("Index");
        }



        public ActionResult EventProfile(int id)
        {
            EventPropHolder model = new EventPropHolder();

            using (var context = new AzureTentanEntities())
            {
                var commentsOfSelectedEvent = context.Comments.Where(c => c.EventId == id).ToList();
                var selectedEvent = context.Events.FirstOrDefault(e => e.ID == id);
                var locationOfSelected = context.Locations.FirstOrDefault(l => l.ID == selectedEvent.LocationID);

                var nrOfLikes = context.Likes.Where(l => l.EventId == id).Count();
                var nrOfAttenders = context.UserEvents.Where(a => a.EventID == id).Count();

                if (locationOfSelected != null)
                {
                    model.City = locationOfSelected.City;
                    model.Street = locationOfSelected.Address;
                }

                if (commentsOfSelectedEvent != null)
                {
                    model.ListOfComments = new List<CommentsDisplayModel>();

                    foreach (var item in commentsOfSelectedEvent)
                    {
                        var commentToAdd = new CommentsDisplayModel
                        {
                            CommentId = item.ID,
                            Comment = item.Content,
                            CommentOwner = item.Username
                        };
                        model.ListOfComments.Add(commentToAdd);
                    }

                }
                var currentUserId = context.Users.FirstOrDefault(u => u.UserName == User.Identity.Name);

                model.UserId = currentUserId.ID;
                model.UserName = User.Identity.Name;
                model.EventId = id;
                model.Name = selectedEvent.Name;
                model.Description = selectedEvent.Description;
                model.PictureUrl = selectedEvent.Picture;
                model.Attenders = nrOfAttenders;
                model.Date = selectedEvent.Date;
                model.Likes = nrOfLikes;           
            }

            return PartialView("_EventProfile", model);
        }

        [HttpGet]
        public ActionResult Contact()
        {

            return View();
        }

     
        public void Contact(string name, string mail, string message)
        {
            
                 try
                {
                    string email = "piplwithfomo@gmail.com";
                    string password = "Hejsan123";

                    var loginInfo = new NetworkCredential(email, password);
                    var msg = new MailMessage();
                    var smtpClient = new SmtpClient("smtp.gmail.com", 587);

                    msg.To.Add(new MailAddress("piplwithfomo@gmail.com"));
                    msg.From = new MailAddress(mail);
                    msg.Subject = "Mail från: " + name;
                    msg.Body = message;
                    msg.IsBodyHtml = true;

                    smtpClient.EnableSsl = true;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = loginInfo;
                    smtpClient.Send(msg);
                }
                catch (Exception)
                {


                }
            
          
        }


        public void SendInvite(string reciver, int id)
        {
            using (var context = new AzureTentanEntities())
            {
                var currentEvent = context.Events.FirstOrDefault(e => e.ID == id);
                string eventName = currentEvent.Name;
                string url = "http://fomo.azurewebsites.net/";

                string message = "Gå in och registrera dig på " + url + " och checka in detta eventet ---->" + eventName + Environment.NewLine + "Hoppas vi ses där!";
                try
                {
                    string email = "piplwithfomo@gmail.com";
                    string password = "Hejsan123";

                    var loginInfo = new NetworkCredential(email, password);
                    var msg = new MailMessage();
                    var smtpClient = new SmtpClient("smtp.gmail.com", 587);

                    msg.To.Add(new MailAddress(reciver));
                    msg.From = new MailAddress(email);
                    msg.Subject = "FOMO? Missa då inte detta event!" + eventName + "!";
                    msg.Body = message;
                    msg.IsBodyHtml = true;

                    smtpClient.EnableSsl = true;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = loginInfo;
                    smtpClient.Send(msg);
                }
                catch (Exception)
                {


                }
            }
          
        }

        public void PostComment(string comment, int eventId)
        {
            string currentUser;
          
            using (var context = new AzureTentanEntities())
            {
               
                    currentUser = User.Identity.Name;               
               var commentOwner = context.Users.FirstOrDefault(u => u.UserName == currentUser);

                context.Comments.Add(new Comment 
                {
                    Content = comment,
                    EventId = eventId,
                    UserId = commentOwner.ID,
                    Username = commentOwner.UserName
                });
                context.SaveChanges();
            }
        }
    }
}