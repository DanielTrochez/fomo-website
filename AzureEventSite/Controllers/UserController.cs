﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AzureEventSite.Models;
using System.Web.Security;
using AzureEventSite.UserHandlerServiceReference1;

namespace AzureEventSite.Controllers
{
    public class UserController : Controller
    {
        private UserService1Client proxy;

        public UserController()
        {
            proxy = new UserService1Client();
        }
        
        public ActionResult Index()
        {        

            return View();
        }

        [HttpGet]
        public ActionResult LogIn()
        {
            return PartialView("_LogIn");
        }

        [HttpPost]
        public ActionResult LogIn(UserModel user)
        {
            if (ModelState.IsValid)
            {
                if (proxy.IsLogInValid(user.UserName, user.PassWord))
                {

                    FormsAuthentication.SetAuthCookie(user.UserName, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Fel användarnamn eller lösenord");
                }             
            }

            return View(user);
        }

        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Registration(UserModel user)
        {
            if (ModelState.IsValid)
            {
                var newUser = new RegisterModel { FirstName = user.FirstName, LastName = user.LastName, UserName = user.UserName, PassWord = user.PassWord };
                if (proxy.IsRegistrationValid(newUser))
                {
                    return RedirectToAction("Index", "Home");
                }             
            }

            return View(user);
        }

        public ActionResult LogOut()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

	}
}