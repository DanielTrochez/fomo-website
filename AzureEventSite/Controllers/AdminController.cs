﻿using AzureEventSite.Helpers;
using AzureEventSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.WindowsAzure.Storage;

namespace AzureEventSite.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                using (var context = new AzureTentanEntities())
                {
                    string currentUserName = User.Identity.Name;
                    var currentuser = context.Users.FirstOrDefault(u => u.UserName == currentUserName);

                    if (currentuser.IsAdmin)
                    {
                        List<EventDisplayModel> model = new List<EventDisplayModel>();

                        var allEvents = context.Events.ToList();   

                        foreach (var item in allEvents)
                        {
                            var publisher = context.Users.FirstOrDefault(u => u.ID == item.PublisherID);

                            var eventToAdd = new EventDisplayModel                            
                            { 
                                Id = item.ID,
                                Name = item.Name,
                                Attenders = item.Attenders,
                                NonAttenders = item.NonAttenders,
                                Likes = item.Likes,
                                Dislikes = item.Dislikes,
                                Date = item.Date,
                                Creater = publisher.UserName,                      
                            };

                            model.Add(eventToAdd);
                        }
                        
                        return PartialView("_Admin", model);
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }


        public ActionResult ShowComments()
        {
            List<CommentsDisplayModel> model = new List<CommentsDisplayModel>();

            using (var context = new AzureTentanEntities())
            {
               

                var allComments = context.Comments.ToList();

                foreach (var item in allComments)
                {
                    var commentToAdd = new CommentsDisplayModel 
                    {
                     CommentId = item.ID,
                     Comment = item.Content,
                     EventId = item.EventId,
                     CommentOwner = item.Username
                    };
                    model.Add(commentToAdd);
                }
            }
            return PartialView("_ShowComments", model);
        }

        public ActionResult ShowTable()
        {
            var account = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=mittstoragetest;AccountKey=kxg+pXxFk0nVmYdM/BiUKtOXlLhR6jOgzqiPpj/QLImAY06M5bbAu/KR8tzLy8xZpg6548DNLjBzxqkYdWLZww==");

             var tableStorage = account.GetLogs(User.Identity.Name);
             List<LogModel> listOfTableStorage = new List<LogModel>();

             foreach (var item in tableStorage)
             {
                 var logToAdd = new LogModel 
                 {
                    EventName = item.EventName,
                    TimeOfEvent = item.TimeOfEvent,
                    Timestamp = item.Timestamp,
                    UserName = item.UserName,
                 };

                 listOfTableStorage.Add(logToAdd);
             }

             return PartialView("_ShowTable", listOfTableStorage);
        }
	}
}