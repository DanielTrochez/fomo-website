﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AzureEventSite.Models;
using AzureEventSite.Helpers;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Twilio;

namespace AzureEventSite.Controllers
{
    public class EventController : ApiController
    {
        

        [HttpGet]
        public IEnumerable<Event> Get()
        {
            return GetEventsFromDB();
        }


        [HttpGet]
        public Event Get(int id)
        {
            var events = GetEventsFromDB();

            return events.FirstOrDefault(e => e.ID == id);
        }

        [HttpPost]
        public void Post(EventPropHolder model)
        {
            var account = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=mittstoragetest;AccountKey=kxg+pXxFk0nVmYdM/BiUKtOXlLhR6jOgzqiPpj/QLImAY06M5bbAu/KR8tzLy8xZpg6548DNLjBzxqkYdWLZww==");


            BlobStorageHelper helper = new BlobStorageHelper();
            Uri url;
            using (var context = new AzureTentanEntities())
            {
                if (model.Picture != null)
                {
                    if (model.Picture.ContentLength > 0)
                    {
                        CloudBlobContainer blobContainer = helper.GetCloudBlobContainer();
                        CloudBlockBlob blob = blobContainer.GetBlockBlobReference(model.Picture.FileName);
                        blob.UploadFromStream(model.Picture.InputStream);

                        url = blob.Uri;
                        model.PictureUrl = url.OriginalString;
                    }
                }


                var newEventLocation = context.Locations.Add(new Location { City = model.City, Address = model.Street });
                context.SaveChanges();

                var publisher = context.Users.FirstOrDefault(p => p.UserName == User.Identity.Name);

                context.Events.Add(new Event
                {
                    Name = model.Name,
                    Description = model.Description,
                    Date = model.Date,
                    Picture = model.PictureUrl,
                    PublisherID = publisher.ID,
                    LocationID = newEventLocation.ID,

                });
                context.SaveChanges();

                var logToUpload = new LogModel { EventName = model.Name, UserName = User.Identity.Name, TimeOfEvent = DateTime.Now };
                account.UploadToTable(logToUpload);

                string userName = User.Identity.Name;
                SendTextMessage(model.Name, userName);
            }
        }

        [HttpPut]
        public bool Put(EventPropHolder model)
        {
            bool returnValue = false;
            using (var context = new AzureTentanEntities())
            {
                var eventToUpdate = context.Events.FirstOrDefault(e => e.ID == model.EventId);

                if (model.Attenders != null)
                {
                    var isUserAttending = context.UserEvents.FirstOrDefault(e => e.EventID == model.EventId && e.UserID == model.UserId);

                    if (isUserAttending == null)
                    {
                        context.UserEvents.Add(new UserEvent { EventID = model.EventId, UserID = model.UserId });
                        eventToUpdate.Attenders += model.Attenders;
                        context.SaveChanges();

                        returnValue = false;
                    }
                    else
                    {
                        returnValue = true;
                    }
                }

                if (model.NonAttenders != null)
                {
                    var isUserNotAttending = context.UserEvents.FirstOrDefault(e => e.EventID == model.EventId && e.UserID == model.UserId);

                    if (isUserNotAttending != null)
                    {
                        context.UserEvents.Remove(isUserNotAttending);
                        eventToUpdate.NonAttenders += model.NonAttenders;
                        context.SaveChanges();

                        returnValue = true;
                    }
                    else
                    {
                        returnValue = false;
                    }

                }

                if (model.Likes != null)
                {
                    var isUserLikeing = context.Likes.FirstOrDefault(l => l.EventId == model.EventId && l.UserId == model.UserId);
                    if (isUserLikeing == null)
                    {
                        context.Likes.Add(new Like { EventId = model.EventId, UserId = model.UserId });
                        eventToUpdate.Likes += model.Likes;
                        context.SaveChanges();
                        returnValue = false;
                    }
                    else
                    {
                        returnValue = true;
                    }

                }

                if (model.Dislikes != null)
                {
                    var isUserLikeing = context.Likes.FirstOrDefault(l => l.EventId == model.EventId && l.UserId == model.UserId);
                    if (isUserLikeing != null)
                    {
                        context.Likes.Remove(isUserLikeing);
                        eventToUpdate.Dislikes += model.Dislikes;
                        context.SaveChanges();

                    }

                }
           
                return returnValue;

            }
        }

        [HttpDelete]
        public void Delete(EventPropHolder model)
        {
            using (var context = new AzureTentanEntities())
            {

                if (model.EventId != 0)
                {
                    var eventToDelete = context.Events.FirstOrDefault(e => e.ID == model.EventId);
                    var eventLocationToDelete = context.Locations.FirstOrDefault(l => l.ID == eventToDelete.LocationID);
                    var belongingCommentsToDelete = context.Comments.Where(c => c.EventId == model.EventId).ToList();
                    var eventAttenders = context.UserEvents.FirstOrDefault(e => e.EventID == model.EventId);
                    var eventLikes = context.Likes.FirstOrDefault(l => l.EventId == model.EventId);

                    if (belongingCommentsToDelete.Count != 0)
                    {
                        foreach (var item in belongingCommentsToDelete)
                        {
                            context.Comments.Remove(item);
                        }
                    }
                    
                    if (eventLikes != null)
                    {
                        context.Likes.Remove(eventLikes);
                    }

                    if (eventAttenders != null)
                    {
                        context.UserEvents.Remove(eventAttenders);
                    }

                    if (eventLocationToDelete != null)
                    {
                        context.Locations.Remove(eventLocationToDelete);
                    }
                 
                    context.Events.Remove(eventToDelete);
                    context.SaveChanges();
                }

                if (model.CommentId != 0)
                {
                    var commentToDelete = context.Comments.FirstOrDefault(c => c.ID == model.CommentId);
                    context.Comments.Remove(commentToDelete);
                    context.SaveChanges();
                }



            }
        }

        private List<Event> GetEventsFromDB()
        {
            List<Event> events = new List<Event>();

            using (var context = new AzureTentanEntities())
            {
                var allEvents = context.Events;

                foreach (var e in allEvents)
                {
                    var eventToAdd = new Event
                    {
                        ID = e.ID,
                        Name = e.Name,
                        Description = e.Description,
                        Picture = e.Picture,
                        SentInvites = e.SentInvites,
                        Attenders = e.Attenders,
                        NonAttenders = e.NonAttenders,
                        PublisherID = e.PublisherID,
                        Location = e.Location
                    };

                    events.Add(eventToAdd);
                }
                return events;
            }
        }

        public void SendTextMessage(string eventName, string userName)
        {
            TwilioRestClient client = new TwilioRestClient("AC9aec39c0e19018cec20abc2f4951dbf0", "f2afdd2f81d86915116b9d59daa59672");

            string message = "Ett nytt event med namn: " + eventName + " har skapats upp utav användarnamnet: " + userName;

            client.SendMessage("(617) 606-9583", "+46760320726", message);
        }
    }
}
