﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace AzureEventSite.Helpers
{
    public class BlobStorageHelper
    {
        public  CloudBlobContainer GetCloudBlobContainer()
        {
            CloudStorageAccount account = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=eventgalleri;AccountKey=8L8T8dofElUA/OMi5RIuzOCrO4cbV34ad//2z1yfd0Ni2busmZuSN0Ei3lS7/BpzVUpvwlC+mGjwaS2D4FRgQg==");

            CloudBlobClient blobClient = account.CreateCloudBlobClient();
            CloudBlobContainer blobContainer = blobClient.GetContainerReference("eventgallery");
            if (blobContainer.CreateIfNotExists())
            {
                blobContainer.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });
            }
            return blobContainer;
        }
       
    }
}