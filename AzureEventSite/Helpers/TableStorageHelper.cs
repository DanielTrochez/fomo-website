﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using AzureEventSite.Models;


namespace AzureEventSite.Helpers
{
    public static class TableStorageHelper
    {
        public static void UploadToTable(this CloudStorageAccount account, LogModel model)
        {
            CloudTableClient client = account.CreateCloudTableClient();
            CloudTable table = client.GetTableReference("myeventtable");
            table.CreateIfNotExists();

            var log = CreateLogForEvent(model);

            TableOperation operation = TableOperation.Insert(log);
            table.Execute(operation);
        }

        private static LogModel CreateLogForEvent(LogModel model)
        {
            return new LogModel(model.EventName, model.UserName) { EventName = model.EventName, TimeOfEvent = model.TimeOfEvent, UserName = model.UserName };
        }

        public static IEnumerable<LogModel> GetLogs(this CloudStorageAccount account, string rowKey)
        {
            var client = account.CreateCloudTableClient();
            var table = client.GetTableReference("myeventtable");
         
            TableQuery<LogModel> query = new TableQuery<LogModel>().Take(10);
         

            List<LogModel> logList = new List<LogModel>();

            foreach (var item in table.ExecuteQuery(query))
            {
                logList.Add(item);
            }
           
            return logList;
        }


    }
}