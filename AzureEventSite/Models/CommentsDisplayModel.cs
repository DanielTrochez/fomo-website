﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzureEventSite.Models
{
    public class CommentsDisplayModel
    {
        public int CommentId { get; set; }
        public string Comment { get; set; }
        public string CommentOwner { get; set; }
        public int EventId { get; set; }
    }
}