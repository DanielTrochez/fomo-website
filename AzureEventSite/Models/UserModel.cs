﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AzureEventSite.Models
{
    public class UserModel
    {
        [Required]
        [Display(Name="Användarnamn: ")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(20, MinimumLength=6)]
        [Display(Name = "Lösenord: ")]
        public string PassWord { get; set; }

    
        [Display(Name="Förnamn")]
        public string FirstName { get; set; }
        
      
        [Display(Name="Efternamn")]
        public string LastName { get; set; }

    }
}