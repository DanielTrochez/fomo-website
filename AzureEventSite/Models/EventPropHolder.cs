﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzureEventSite.Models
{
    public class EventPropHolder
    {
        public int EventId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public HttpPostedFileBase Picture { get; set; }
        public string PictureUrl { get; set; }
        public int SentInvites { get; set; }
        public int? Attenders { get; set; }
        public int? NonAttenders { get; set; }
        public string Date { get; set; }
        public int? Likes { get; set; }
        public int? Dislikes { get; set; }
        public int UserId { get; set; }

        public int PublisherId { get; set; }
        public int LocationId { get; set; }

        //Event loaction
        public int Zipcode { get; set; }
        public string Street { get; set; }
        public string City { get; set; }

        //EventComments   
        public int CommentId { get; set; }
        public string UserName { get; set; }
        public List<CommentsDisplayModel> ListOfComments { get; set; }

    }
}