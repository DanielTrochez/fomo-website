﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzureEventSite.Models
{
    public class EventDisplayModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Pic { get; set; }
        public string Date { get; set; }

        //Admin
        public int? Attenders { get; set; }
        public int? NonAttenders { get; set; }
        public int? Likes { get; set; }
        public int? Dislikes { get; set; }
        public string Creater { get; set; }
        public DateTime TimeEventGotCreated { get; set; }


    }
}