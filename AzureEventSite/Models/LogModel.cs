﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AzureEventSite.Models
{
    public class LogModel : TableEntity
    {
        public string EventName { get; set; }
        public DateTime TimeOfEvent { get; set; }
        public string UserName { get; set; }

        public LogModel(string name, string UserName)
        {
            this.PartitionKey = name;
            this.RowKey = UserName;
        }

        public LogModel()
        {
        }
    }
}